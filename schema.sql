-- sql käsud tabelite tekitamiseks

create table contact (
    id              int primary key auto_increment,
    first_name  varchar(255),
    last_name           varchar(255)
);

create table phone_number (
    id          int primary key auto_increment,
    contact_id  int,
    phone_number int
);
