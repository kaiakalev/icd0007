<?php

require_once 'Vendor/tpl.php';
require_once 'Person.php';
require_once 'PersonList.php';

$cmd = "show_list_page";
if (isset($_GET["cmd"])) {
    $cmd = $_GET["cmd"];
}


if ($cmd === 'show_list_page') {
    $contacts = getContacts();
    $data = [
        'title' => 'List page',
        'template' => 'list.html',
        'people' => $contacts,
        'cmd' => 'show_list_page',
    ];
    print renderTemplate('templates/main.html', $data);

} else if ($cmd === 'show_add_page') {
    $data = [
        'title' => 'Add new entry',
        'template' => 'add.html',
        'cmd' => 'add',
    ];

    print renderTemplate('templates/main.html', $data);
} else if ($cmd === "add") {
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $phone1 = $_POST['phone1'];
    $phone2 = $_POST['phone2'];
    $phone3 = $_POST['phone3'];
    $contact = new Person($firstName, $lastName);

    $contact->addPhones($phone1);
    $contact->addPhones($phone2);
    $contact->addPhones($phone3);

    $errors = $contact->validate();
    if (count($errors) > 0) {
        $data = [
            'title' => ' entry',
            'template' => 'add.html',
            'cmd' => 'add',
            'person' => $contact,
            'phone1' => ($contact->phones)[0],
            'phone2' => ($contact->phones)[1],
            'phone3' => ($contact->phones)[2],
            'errors' => $errors
        ];
        print renderTemplate('templates/main.html', $data);
    } else {
        addContact($contact);

        $contracts = getContacts();

        $data = [
            'template' => 'list.html',
            'cmd' => 'show_list_page',
            'people' => $contracts,
        ];
        print renderTemplate('templates/main.html', $data);
    }
} else if ($cmd === "show_edit_page") {

    $id = $_GET["id"];

    $contact = getContractById($id);

    $data = [
        'title' => ' entry',
        'template' => 'edit.html',
        'cmd' => 'add',
        'person' => $contact,
        'phone1' => ($contact->phones)[0],
        'phone2' => ($contact->phones)[1],
        'phone3' => ($contact->phones)[2],
    ];
    print renderTemplate('templates/main.html', $data);

} else if ($cmd === "edit") {
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $phone1 = $_POST['phone1'];
    $phone2 = $_POST['phone2'];
    $phone3 = $_POST['phone3'];
    $id = $_POST["id"];
    $contact = new Person($firstName, $lastName, $id);

    $contact->addPhones($phone1);
    $contact->addPhones($phone2);
    $contact->addPhones($phone3);

    $errors = $contact->validate();

    if (count($errors) > 0) {
        $data = [
            'title' => ' entry',
            'template' => 'edit.html',
            'cmd' => 'add',
            'person' => $contact,
            'phone1' => ($contact->phones)[0],
            'phone2' => ($contact->phones)[1],
            'phone3' => ($contact->phones)[2],
            'errors' => $errors
        ];
        print renderTemplate('templates/main.html', $data);
    } else {
        editContact($contact);

        $contracts = getContacts();

        $data = [
            'template' => 'list.html',
            'cmd' => 'show_list_page',
            'people' => $contracts,
        ];
        print renderTemplate('templates/main.html', $data);
    }
} else {
    header("Location: http://localhost:8080/");
    exit();
}








