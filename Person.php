<?php


class Person {
    public $id;
    public $firstName;
    public $lastName;
    public $phones;


    public function __construct($firstName, $lastName, $id = null) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->id = $id;
        $this->phones = [];
    }

    public function addPhones($phone) {
        $this->phones[] = $phone;
    }

    public function validate() {
        $errors = [];

        if (strlen($this->firstName)<2) {
            $errors[] = "Eesinimi peab olema vähemalt kaks tähemärki pikk";
        }

        if (strlen($this->lastName)<2) {
            $errors[] = "Perekonnanimi peab olema vähemalt kaks tähemärki pikk";
        }
        return $errors;
    }
}
