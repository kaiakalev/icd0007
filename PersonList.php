<?php

require_once("Person.php");

const USERNAME = 'kaiakalev';
const PASSWORD = 'd068';
//const URL = 'jdbc:mysql://db.mkalmo.xyz:3306';
const URL = 'mysql:host=db.mkalmo.xyz:3306;dbname=kaiakalev';



function addContact($contact) {

    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $statement = $connection->prepare("insert into contacts (name, last_name)
    values (:name, :last_name)");

    $statement->bindValue(":name", $contact->firstName);
    $statement->bindValue(":last_name", $contact->lastName);
    $statement->execute();

    $contactId = $connection->lastInsertId();

    foreach ($contact->phones as $phone) {
        $statement = $connection->prepare("insert into phones (contact_id, phone_nr) 
            values(:Id, :phone)");
        $statement->bindValue(":Id", $contactId);
        $statement->bindValue(":phone", $phone);
        $statement->execute();
    }


}

function getContractById($id) {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("select id, name, last_name, phone_nr from contacts
        left join phones on contact_id = id where id = :id");
    $statement->bindValue(":id", $id);
    $statement->execute();


    $contact = null;
    foreach ($statement as $row) {
        $id = $row["id"];

        if (isset($contact)) {
            $phone = $row["phone_nr"];
            $contact->addPhones($phone);
        } else {
            $name = $row["name"];
            $lastName = $row["last_name"];
            $phone = $row["phone_nr"];
            $contact = new Person($name, $lastName, $id);
            $contact->addPhones($phone);
        }
    }

    return $contact;
}

function getContacts() {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("select id, name, last_name, phone_nr from contacts
        left join phones on contact_id = id order by id asc");
    $statement->execute();

    $contacts = [];
    foreach ($statement as $row) {
        $id = $row["id"];

        if (isset($contacts[$id])) {
            $contact = $contacts[$id];
            $phone = $row["phone_nr"];
            $contact->addPhones($phone);
        } else {
            $name = $row["name"];
            $lastName = $row["last_name"];
            $phone = $row["phone_nr"];
            $contact = new Person($name, $lastName, $id);
            $contact->addPhones($phone);
        }

        $contacts[$id] = $contact;
    }

    return array_values($contacts);
}

function deleteContact($id) {

    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $statement = $connection->prepare(
        "delete from contacts where id = :id");

    $statement->bindValue(":id", $id);
    $statement->execute();
}

function deletePhones($id) {
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $statement = $connection->prepare(
        "delete from phones where contact_id = :id");
    $statement->bindValue(":id", $id);
    $statement->execute();
}

function editContact($person) {
    deletePhones($person->id);
    $connection = new PDO(URL, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $statement = $connection->prepare("update contacts set name= :name, last_name = :last_name
     where id = :id ");

    $statement->bindValue(":name", $person->firstName);
    $statement->bindValue(":last_name", $person->lastName);
    $statement->bindValue(":id", $person->id);
    $statement->execute();

    foreach ($person->phones as $phone) {
        $statement = $connection->prepare("insert into phones (contact_id, phone_nr) 
            values(:Id, :phone)");
        $statement->bindValue(":Id", $person->id);
        $statement->bindValue(":phone", $phone);
        $statement->execute();
    }

}
